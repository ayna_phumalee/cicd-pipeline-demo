package com.example.cicdpipelinedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CicdPipelineDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdPipelineDemoApplication.class, args);
	}

}
