package com.example.cicdpipelinedemo.controller;

import com.example.cicdpipelinedemo.response.ResultResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/demo/v1")
public class IndexController {

    @GetMapping("")
    public ResponseEntity<ResultResponse> getHello(){
        ResultResponse resultResponse = ResultResponse.builder()
                .title("CI/CD Pipeline Success")
                .code(HttpStatus.OK)
                .data("Hello Sprint - CI/CD Pipeline Demo")
                .build();
        return ResponseEntity.ok(resultResponse);
    }
}
