pipeline {
       environment{
           CONTAINER_NAME = "cicd-pipeline-demo"
           DOCKER_IMAGE_NAME = "cicd-pipeline-demo"
           DEPLOYMENT_SERVER_USER = "jenkins"
           DEPLOYMENT_SERVER_CONFIG_NAME = "Deployment Server"
           DEPLOYMENT_SERVER_PRIVATE_KEY = credentials("deployment-server-private-key")
       }
       agent {
           docker {
               image "maven:3.6.2-jdk-8"
           }
       }
       stages{
           stage("Build") {
               steps {
                   sh "mvn clean package"
               }
           }
           stage("Unit Test") {
                steps {
                   sh "mvn test"
                }
                post {
                    always {
                        junit "target/surefire-reports/*.xml"
                    }
                }
           }
           stage("Docker Build Image") {
                 steps {
                     sh """[ -z \$(docker images -q ${DOCKER_IMAGE_NAME}) ] || \
                           docker rmi \$(docker images | grep '${DOCKER_IMAGE_NAME}' | awk '{ print \$3 }')"""
                     sh "docker build -f Dockerfile -t ${DOCKER_IMAGE_NAME}:${BUILD_NUMBER} ./"
                     sh "docker save -o ${DOCKER_IMAGE_NAME}:${BUILD_NUMBER}.tar ${DOCKER_IMAGE_NAME}:${BUILD_NUMBER}"
                 }
           }
           stage("Transfer Image to Environment Deployment") {
                steps {
                    sshPublisher(
                        publishers: [
                            sshPublisherDesc(
                                configName: "${DEPLOYMENT_SERVER_CONFIG_NAME}",
                                sshCredentials: [keyPath: "${DEPLOYMENT_SERVER_PRIVATE_KEY}", username: "${DEPLOYMENT_SERVER_USER}"],
                                transfers: [
                                    sshTransfer(
                                        sourceFiles: "${DOCKER_IMAGE_NAME}:${BUILD_NUMBER}.tar",
                                        execCommand: "docker load -i ${DOCKER_IMAGE_NAME}:${BUILD_NUMBER}.tar"
                                    )
                                 ],
                                verbose: true
                            )
                        ]
                    )
                }
           }
           stage("Docker Deploy to Environment Deployment") {
                 steps {
                    sshPublisher(
                     publishers: [
                         sshPublisherDesc(
                             configName: "${DEPLOYMENT_SERVER_CONFIG_NAME}",
                             sshCredentials: [keyPath: "${DEPLOYMENT_SERVER_PRIVATE_KEY}", username: "${DEPLOYMENT_SERVER_USER}"],
                             transfers: [
                                 sshTransfer(
                                     execCommand: """
                                        [ -z \$(docker ps -a | grep '${CONTAINER_NAME}') ] || docker stop \$(docker ps -a | grep '${CONTAINER_NAME}' | awk '{ print \$1 }') \n
                                        [ -z \$(docker ps -a | grep '${CONTAINER_NAME}') ] || docker container rm \$(docker ps -a | grep '${CONTAINER_NAME}' | awk '{ print \$1 }') \n
                                        rm ./${DOCKER_IMAGE_NAME}:${BUILD_NUMBER}.tar \n
                                        docker run -p 8000:8081 -d --name ${CONTAINER_NAME} ${DOCKER_IMAGE_NAME}:${BUILD_NUMBER} \n
                                     """
                                 )
                              ],
                             verbose: true
                         )
                     ]
                   )
                 }
           }
       }
}




