FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/cicd-pipeline-demo-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} app.jar
EXPOSE 8081
ENTRYPOINT ["java","-jar","app.jar"]
